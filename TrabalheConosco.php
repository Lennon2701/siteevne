<?php
require("head.php");
include("menu.php");
?>
<script>
    let menu = [document.getElementById('servicosMenu'),
        document.getElementById('faleConoscoMenu'),
        document.getElementById('centralDoCliente')
    ];
    for (var i = 0; i < menu.length; i++) {
        menu[i].hidden = true;
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<body id="page-top">
    <header class="trabalheConosco">
        <div class="intro-text">
            <div class=" font-weight-bold display-4 text-uppercase" style="color: #230241;">
                VENHA TRABALHAR CONOSCO!
            </div>
        </div>
    </header>
    <div class=" font-weight-bold display-7 fluid-container text-uppercase" style="color: #230241;">Oque esperamos de quem
        trabalha conosco
    </div>
    <div class="container-fluid">
        <!--<div class="intro-lead-in display-4">-->
        <li>Que ENTENDAM o cliente e sintam prazer em vê-lo FELIZ!</li>
        <li>Que tenham automotivação, energia e ALEGRIA.</li>
        <li>Que tenham CURIOSIDADE e que gostem de APRENDER coisas novas.</li>
        <li>Que adorem tecnologia e consigam ser AUTODIDATAS no assunto.</li>
        <li>Que AJUDEM A EVOLUIR nossas aplicações com base no feedback dos clientes.</li>
        <!--</div>-->
    </div>
    <p/>
    <br/>
    <br/>
    <br/>
    <div id="pre_vendedor">
        <div class="container-fluid">
            <h2>Pré-vendedor(a)</h2>
            <li>Modelo de contratação: CLT Carga Horária: 44 horas semanais</li>
        </div>
        <p/>
        <div class="container-fluid">
            <h4>Principais atribuições:</h4>
            <li>Qualificar os Leads gerados e garantir que essas oportunidades estejam alinhadas à régua de qualificação definida pela empresa.
            </li>
            <li>Gerar valor em uma conexão rápida para que o Lead engaje-se no processo comercial e se interesse em conversar com o time de vendas.
            </li>
            <p/>
        </div>
        <div class="container-fluid">
            <h4>Competências necessárias:</h4>
            <li>Trabalhar com volume de ligações telefônicas.</li>
            <li>Ser organizado(a).</li>
            <li>Ser resiliente.</li>
            <li>Criar abordagens replicáveis.</li>
        </div>
        <p/>
    </div>
    <div id="vendedor">
        <div class="container-fluid">
            <h2>Vendedor</h2>
            <li>Modelo de contratação: CLT Carga Horária: 44 horas semanais</li>
            <p/>
            <h4>Principais atribuições:</h4>
            <li>Levantamento de necessidades, apresentação de soluções e elaboração de Orçamentos. Negociação de vendas.</li>
            <p/>
            <h4>Competências necessárias:</h4>
            <li>Excelente comunicação, dinâmico e proativo.</li>
            <li>Bom conhecimento em ferramentas Microsoft Office.</li>
            <li>Experiência em vendas consultivas B2B, de preferência em TI.</li>
            <li>Conhecimento em gestão empresarial.</li>
            <li>Questionador, interessado, atento aos detalhes.</li>
            <li>Ambição por crescimento profissional e financeiro.</li>
            <br>
        </div>
    </div>
    <br/>
    <br/>
    <div id="suporte_tecnico">
        <div class="container-fluid">
            <h2>Suporte Técnico</h2>
            <li>Modelo de contratação: CLT ou Estágio remunerado Carga Horária: 20 ou 44 horas semanais</li>
            <p/>
            <h4>Principais atribuições:</h4>
            <li>Atendimento a clientes (e-mail, chat e telefone).</li>
            <li>Auxilio na construção de Aplicativos.</li>
            <li>Elaborar documentação de ajuda para as aplicações.</li>
            <p/>
            <h4>Competências necessárias:</h4>
            <li>Boa comunicação, escrita e falada.</li>
            <li>Ser organizado(a).</li>
            <li>Ser resiliente.</li>
            <li>Ter proatividade e curiosidade.</li>
        </div>
        <p/>
    </div>
    <div id="programador">
        <div class="container-fluid">
            <h2>Programador</h2>
            <li>Modelo de contratação: CLT Carga Horária: 44 horas semanais</li>
            <p/>
            <h4>Principais atribuições:</h4>
            <p/>
            <li>Levantamento de requisitos e desenho de processos.</li>
            <li>Criação de Aplicativos utilizando plataforma low-code.</li>
            <li>Desenvolvimento de integrações entre sistemas.</li>
            <li>Elaborar documentação de ajuda para as aplicações.</li>
            <p/>
            <h4>Conhecimento desejável:</h4>
            <p>
            <li>Lógica de programação.</li>
            <li>Processos de negócio.</li>
            <li>Linguagens PHP ou Java.</li>
            <li>Linux.</li>
            <li>Noções de Banco de Dados.</li>
        </div>
    </div>
    <section id="contact" class="container-fluid center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-dark">Trabalhe Conosco</h2>
                    <h3 class="section-subheading text-dark"></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-1">
                    <form id="contactForm" name="sentMessage" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" id="nome" type="text" placeholder="Seu nome"
                                           required="required"
                                           data-validation-required-message="Por favor, adicione seu nome">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="email" type="email" placeholder="Seu email"
                                           required="required">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="phone" type="tel" placeholder="Seu telefone"  required="required" data-validation-required-message="Por favor, adicione o seu telefone">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="curriculo" type="text" placeholder="Link para seu Curriculo"
                                           required="required"
                                           data-validation-required-message="Por favor  adicione o link do seu Currículo">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="endereco" type="text" placeholder="Endereço"
                                           required="required"
                                           data-validation-required-message="Por favor adicione seu endereço">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="data" type="date" placeholder="Data de Nascimento" required="required" data-validation-required-message="Por favor sua Data de Nascimento">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div clas
                                     <div class="form-grSelect listoup">
                                        <label>Vaga desejada</label>
                                        <select id="vaga" class="form-control" >
                                            <option value = "suporte">Suporte Técnico</option>
                                            <option value = "programador">Programador</option>
                                            <option value = "assistente">Assistente Comercial</option>
                                            <option value = "vendas">Executivo de Vendas</option>
                                        </select>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <!--                                <div class="form-group">-->
                                    <!--                                    <input class="form-control" id="curriculoArquivo" type="file" placeholder="Link para seu Curriculo"-->
                                    <!--                                           required="required"-->
                                    <!--                                           data-validation-required-message="Por favor adicione o link do seu Currículo">-->
                                    <!--                                    <p class="help-block text-danger"></p>-->
                                    <!--                                </div>-->
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea class="form-control" id="message" placeholder="Fale um pouco sobre Você" required="required"
                                                  data-validation-required-message="Por favor, entre com a sua mensagem."></textarea>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 text-center">
                                    <div id="success"></div>
                                    <button class="btn btn-primary btn-xl text-uppercase" id="sendMessageButton"  value="Send"
                                            type="submit">Enviar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </section>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me_rh.js"></script>
    <!---->
    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
    <?php require("footer.php"); ?>
    <br>
</body>