<footer>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Desenvolvido por Evne Sistema.</span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline ">
                    <!--            <li class="list-inline-item ">-->
                    <!--              <a href="https://twitter.com/Evne_TI">-->
                    <!--              <img src="img/redeSocial/twitter.png" alt="">-->
                    <!--              </a>-->
                    <!--            </li>-->
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/EvneAPP/">
                            <img src="img/redeSocial/facebook.png" alt="">
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.linkedin.com/company/evneapp/">
                            <img src="img/redeSocial/linkedin.png" alt="">
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://wa.me/554791049421?text=Ol%C3%A1%2C%20gostaria%20de%20saber%20mais%20sobre%20os%20aplicativos%20da%20Evne!%20%F0%9F%98%83" target="_blank">
                            <img src="img/redeSocial/whatsapp.png" alt="">
                        </a>
                    </li>
                    <li class="list-inline-item ">
                        <a href="https://www.instagram.com/evneapp">
                            <img src="img/redeSocial/instagram.png" alt="">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li class="list-inline-item">
                        <a href="TrabalheConosco.php">Trabalhe conosco</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="sobre.php">Conheça mais sobre nós</a>
                    </li>
                    <li class="list-inline-item">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>