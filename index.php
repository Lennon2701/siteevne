<?php
include("menu.php");
include("head.php");
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<body id="page-top">
    <!-- Navigation -->
    <!-- Header -->
    <header class="masthead">
        <div class="container">
            <div class="intro-text">
                <div class=" font-weight-bold display-4 text-uppercase" style="color: #120025;">APLICATIVOS PARA PESSOAS E
                    NEGÓCIOS
                </div>
                <div class="intro-lead-in display-4" style="color:#120025;">Desenvolvidos de acordo com as suas
                    necessidades!
                </div>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Descubra</a>
            </div>
        </div>
    </header>
    <!-- Services -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Nossas Soluções</h2>
                    <br><br>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <img src="img/solucoes/solu1.png" alt="">
                    </span>

                    <h4 class="service-heading">Força de Vendas</h4>
                    <p class="text-muted">Aumente a produtividade da sua equipe de vendas e otimize a sua gestão sobre
                        ela</p>
                    <h6><p><i><a href="http://parceiros.umov.me/evne/"> Saiba Mais</a></i></p></h6>
                </div>
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <img src="img/solucoes/technical-support.png" alt="">
                    </span>
                    <h4 class="service-heading">Ordem de Serviço</h4>
                    <p class="text-muted">Acompanhe o atendimento aos clientes em tempo real e agilize os processos dos seus
                        técnicos.</p>
                    <h6><p><i><a href="http://parceiros.umov.me/evne/ordem-de-servico/">Saiba Mais</a></i></p></h6>
                </div>
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <img src="img/solucoes/delivery-truck.png" alt="">
                    </span>
                    <h4 class="service-heading">Logística</h4>
                    <p class="text-muted">Roteirize entregas e coletas. Acompanhe por geolocalização seu time em campo.</p>
                    <h6><p><i><a href="http://parceiros.umov.me/evne/logistica/">Saiba Mais</a></i></p></h6>
                </div>
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <img src="img/solucoes/smartphone.png" alt="">
                    </span>
                    <h4 class="service-heading">Projetos Mobile</h4>
                    <p class="text-muted">Criamos soluções mobile para automação e gestão sob medida e de forma ágil.</p>
                    <h6><p><i><a href="http://parceiros.umov.me/evne/projetos-de-mobilidade/"> Saiba Mais</a></i></p></h6>
                </div>
            </div>
        </div>
    </section>
    <script>
        javascript:if (window.navigator.vibrate) {
            this.enableVibration = true;
        }
        window.navigator.vibrate([200, 100, 200]);
    </script>
    <!--Mobilidade que gera Resultados-->
    <section id="mobilidade">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Mobilidade que gera Resultados!</h2>
                    <br><br>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/running.png" alt="">
                    </span>
                    <h4 class="service-heading">Fácil e Rápido</h4>
                    <p class="text-dark font-weight-bold ">Descomplicado. Com interface intuitiva, nossos App's são fáceis
                        de compreender e simples de utilizar</p>
                </div>
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/network.png" alt="">
                    </span>
                    <h4 class="service-heading">Fácil Integração</h4>
                    <p class="text-dark font-weight-bold">Acompanhe o atendimento aos clientes em tempo real e agilize os
                        processos dos seus
                        técnicos.</p>
                </div>
                <div class="col-lg-2">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/cloud.png" alt="">
                    </span>
                    <h4 class="service-heading">Ambiente em nuvem </h4>
                    <p class="text-dark font-weight-bold">Roteiriza entregas e coletas e acompanhe por geolocalização seu
                        time em campo</p>
                </div>
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/tools.png" alt="">
                    </span>
                    <h4 class="service-heading">Personalizado</h4>
                    <p class="text-dark font-weight-bold">Nossas soluções podem ser personalizadas com sua identidade
                        visual, assim seus aplicativos ficam com a sua cara.</p>
                </div>
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/no-wifi.png" alt="">
                    </span>
                    <h4 class="service-heading">Operação OFFLINE</h4>
                    <p class="text-dark font-weight-bold">Você pode trabalhar mesmo sem sinal, e quando voltar a uma área
                        com cobertura, a aplicação sincroniza todos os dados.</p>
                </div>
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/android.png" alt="">
                    </span>
                    <h4 class="service-heading">Multiplataforma</h4>
                    <p class="text-dark font-weight-bold">Nossa plataforma opera em mais de 1000 modelos de smartphones e
                        tablets em compatibilidade com sistemas Android e iOS.</p>
                </div>
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/gallery.png" alt="">
                    </span>
                    <h4 class="service-heading">Multimídia</h4>
                    <p class="text-dark font-weight-bold">Capture fotos, faça a leitura de códigos de barras e QRCodes,
                        colete assinaturas, coordenadas de GPS, e muitos mais!</p>
                </div>
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/search.png" alt="">
                    </span>
                    <h4 class="service-heading">Multisegmento</h4>
                    <p class="text-dark font-weight-bold">As soluções Evne atendem as necessidades de qualquer segmento de
                        mercado. Saúde, Educação, Logística, Varejo, Serviços, Indústria e muitos outros.</p>
                </div>
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/conectado.png" alt="">
                    </span>
                    <h4 class="service-heading">Gerenciamento em tempo real</h4>
                    <p class="text-dark font-weight-bold">Disponibilizamos um ambiente web para você gerenciar seu time
                        através do painel de controle e mapas que possibilitarão maior produtividade para sua equipe. Tudo
                        em tempo real!</p>
                </div>
                <div class="col-lg">
                    <span class="fa-stack fa-4x">
                        <img src="img/mobility/coding.png" alt="">
                    </span>
                    <h4 class="service-heading">Customização sem desenvolvimento</h4>
                    <p class="text-dark font-weight-bold">Dispensa conhecimentos de programação, Podemos modificar a sua
                        aplicação quando quiser, sem complicação!</p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<section class="py-5" id="fotos">
    <div class="col-lg-12 text-center">
        <h2 class="section-heading ">Alguns de nossos clientes!</h2>
        <br>
    </div>
    <div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <!-- The slideshow -->
        <div class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="col-xl-3 col-xs-3 col-sm-3 col-md-3">
                        <img src="img/cases/boxtop.jpg">
                    </div>
                    <div class="col-xl-3 col-xs-2 col-sm-3 col-md-3">
                        <img src="img/cases/saboraki.jpg">
                    </div>
                    <div class="col-xl-3 col-xs-2 col-sm-3 col-md-3">
                        <img src="img/cases/mill.png">
                    </div>
                    <div class="col-xl-3 col-xs-2 col-sm-3 col-md-3">
                        <img src="img/cases/unifique.png">
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="col-xl-3 col-xs-2 col-sm-3 col-md-3">
                        <img src="img/cases/menfis.png">
                    </div>
                    <div class="col-xl-3 col-xs-2 col-sm-3 col-md-3">
                        <img src="img/cases/riologic.jpg">
                    </div>
                    <div class="col-xl-3 col-xs-2 col-sm-3 col-md-3">
                        <img src="img/cases/mundo-das-aguas.jpg">
                    </div>
                    <div class="col-xl-3 col-xs-2 col-sm-3 col-md-3">
                        <img src="img/cases/hardsis.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.carousel').carousel({
            interval: 3500
                    window.navigator.vibrate([100, 30, 100, 30, 100, 30, 200, 30, 200, 30, 200, 30, 100, 30, 100, 30, 100]); // Vibrate 'SOS' in Morse.
        }),
    </script>
</section>
<section id="contact" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase text-dark">Fale conosco!</h2>
                <h3 class="section-subheading text-dark">Nos encaminhe uma mensagem e converse com um especialista!</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-1">
                <form id="contactForm" name="sentMessage" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="name" name="name" type="text" placeholder="Seu Nome"
                                       required="required"
                                       data-validation-required-message="Por favor, adicione seu nome">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="email" type="email" placeholder="Seu email"
                                       required="required"
                                       data-validation-required-message="Por favor, adicione seu e-mail">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="phone" type="tel" placeholder="Seu telefone"
                                       required="required"
                                       data-validation-required-message="Por favor, adicione o seu telefone">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" id="message" placeholder="Sua mensagem" required="required"
                                          data-validation-required-message="Por favor, entre com a sua mensagem."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button class="btn btn-primary btn-xl text-uppercase" id="sendMessageButton" value="Send"
                                    type="submit">Enviar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Footer -->
<!-- Portfolio Modals -->
<!-- <script src="https://www.google.com/recaptcha/api.js?render=6Lce6aIUAAAAAAeJLM6DKcMIZ7MLf2F1vetp-9ik"></script>
 <script>
     grecaptcha.ready(function() {
         grecaptcha.execute('6Lce6aIUAAAAAAeJLM6DKcMIZ7MLf2F1vetp-9ik', {action: 'homepage'}).then(function(token) {
         ...
         });
     });
 </script>
-->
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!--Chat movidesk-->
<script type="text/javascript">var mdChatClient = "12345678901234567890123456789012";</script>
<script src="http://chat.movidesk.com/Scripts/chat-widget.min.js"></script>
<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Contact form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>
<!---->
<!-- Custom scripts for this template -->
<script src="js/agency.min.js"></script>
<?php include("footer.php"); ?>
</body>
</html>