<?php

require '../vendor/autoload.php';

$titulo = htmlspecialchars("Formulário do site - Fale com um Vendedor");
$nome = strip_tags(htmlspecialchars($_POST['name']));
$email = strip_tags(htmlspecialchars($_POST['email']));
$telefone = strip_tags(htmlspecialchars($_POST['phone']));
$mensagem = strip_tags(htmlspecialchars($_POST['message']));

/* Para quem: */
$to = "lennon@evne.net.br";

$assunto = $titulo;

$corpo = "<h3>" . utf8_decode($mensagem) . "</h3>" . "<br>" .
        "Nome: " . utf8_decode($nome) . "<br>" .
        "Número de telefone: " . utf8_decode($telefone) . "<br>" .
        "Email: " . utf8_decode($email);

mail($to, $assunto, $corpo, 'From: ' . $email);
