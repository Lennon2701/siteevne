<?php

require '../vendor/autoload.php';

$titulo = htmlspecialchars("Formulário de contato - Trabalhe Conosco");
$nome = strip_tags(htmlspecialchars($_POST['name']));
$email = strip_tags(htmlspecialchars($_POST['email']));
$telefone = strip_tags(htmlspecialchars($_POST['phone']));
$mensagem = strip_tags(htmlspecialchars($_POST['message']));
$formularioCurriculo = strip_tags(htmlspecialchars($_POST['curriculo']));
$data = strip_tags(htmlspecialchars($_POST['data']));
$endereco = strip_tags(htmlspecialchars($_POST['endereco']));
$vaga = strip_tags(htmlspecialchars($_POST['vaga']));
$anexo = $_POST['anexo'];

/* Para quem: */
$to = "lennon@evne.net.br";

$assunto = $titulo;

$corpo = "<h3>" . $mensagem . "</h3>" . "<br>" . "Nome: " . $nome . "<br>" . "Número de telefone: " . $telefone .
        "<br>" . "Email: " . $email . "<br>" . "Link para curriculo: " . $formularioCurriculo . "<br>" .
        "Data de Nascimento " . $data . "<br>" . "Endereço: " . $endereco . "<br>" . "Vaga desejada: " .
        $vaga;

mail($to, $assunto, $corpo, 'From: ' . $email);
