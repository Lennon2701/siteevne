<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php">
        <img src="img/logo.png" style="width: 120px; lenght:51px;" alt="">
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
        data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
        aria-label="Toggle navigation">
        <i class="fas fa-bars"></i>
      </button>
      <div  class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto" id="texte">
          <li class="nav-item" >
            <a class="nav-link js-scroll-trigger" id="servicosMenu" href="#services">Nossas Soluções</a>
          </li>
          <li class="nav-item">
           <!--<a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>-->
          </li>
          <li class="nav-item" >
            <a class="nav-link js-scroll-trigger" id="faleConoscoMenu" href="#contact">Fale Conosco</a>
          </li>
          <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="https://evne.movidesk.com" id="centralDoCliente" target="_blank">Central do Cliente</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
