<?php

include("head.php");
include("menu.php");
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
    let menu = [document.getElementById('servicosMenu'),
        document.getElementById('faleConoscoMenu')];

    for (var i = 0; i < menu.length; i++) {
        menu[i].hidden = true;
    }
</script>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Um pouco da nossa história</h2>
                <h3 class="section-subheading text-muted">Já são 14 anos de vida, mas nos sentimos começando uma nova e grande jornada!</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/1.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2005</h4>
                                <h4 class="subheading">Fundação</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Distribuidora de
                                    softwares para gestão
                                    de empresas!</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/2.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2009</h4>
                                <h4 class="subheading">Nova Marca</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Criação da nova marca da empresa!</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="css/imagem/sebrae.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2016</h4>
                                <h4 class="subheading">Conquista</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Melhor empresa de serviços em TI de SC. 
                                    Reconhecida pelo
                                    Prêmio MPE Brasil
                                    concedido pelo SEBRAE!</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="css/imagem/app_inovacao.png" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2017</h4>
                                <h4 class="subheading">Inovação</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Nos transformamos em
                                    uma Produtora de
                                    Aplicativos para
                                    empresas!</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="css/imagem/evne.png" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2019</h4>
                                <h4 class="subheading">Transformação</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Nova identidade visual
                                    para marcar nosso foco
                                    com a transformação
                                    digital das empresas!</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>Faça parte
                                <br>Da nossa
                                <br>História!</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <br>
    <div class=" col-12">
        <iframe class="timeline-image" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3548.080623464984!2d-49.64884318528522!3d-27.21661841250186!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dfb9a20f3b831d%3A0x37b0e97c5b7fd920!2sEvne+Aplicativos+para+Empresas!5e0!3m2!1spt-BR!2sbr!4v1557425160438!5m2!1spt-BR!2sbr" width="600" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </p>-->
    </div>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me_rh.js"></script>
    <!---->
    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
</section>
<?php

include("footer.php");
?>
